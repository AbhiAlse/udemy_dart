/*
The Number dataTypes are having various property to check the variable
	
Properties : These Properties are only for integer class
	.isOdd
	.isEven
	.isFinite
	.isInfinite
	.isNan
	.isNegative

Methods :
	.abs()
	.floor()
	.truncate()
	.ceil()
	.remainder()
*/
void main(){
	var x=3;
	print(x);
	print("x is Odd : ${x.isOdd}");
	print("x is Even : ${x.isEven}");
	print("x is Finite : ${x.isFinite}");
	print("x is Infinite : ${x.isInfinite}");
	print("x is Nan : ${x.isNaN}");
	print("x is Negative : ${x.isNegative}");
	
	
	print("----------------------");
	
	var y=3.5;
	print(y);
	
	print("y.round() : ${y.round()}");
	print("y.floor() : ${y.floor()}");
	print("y.truncate() : ${y.truncate()}");
	print("y.ceil() : ${y.ceil()}");
	print("y.remainder() : ${y.remainder(x)}");
	
}


