//exception handelling

import 'dart:io';
void main(){
	
	
	while(true){
		print("Enter ypur age");
	
		try{
			var age = num.parse(stdin.readLineSync()!);
			print("Age :$age");
			break;
		}catch(e,s){   //You can specify one or two parameters to catch(). The first is the exception that was thrown, and the second is the stack trace
			print("In valid input");
			print("Error message: $e");
			print("StackTrace: $s");
			print(s.runtimeType);   //stacktrace
			print(e.runtimeType);   //format exception
		}
//To ensure that some code runs whether or not an exception is thrown, use a finally clause. If no catch clause matches the exception, the exception is propagated after the finally clause runs:
		finally{
			print("In finally block");
		}
	}
}

