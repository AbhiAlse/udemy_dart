void main(){
	var x;
	print(x);  //null becoz:here null value can also be
	
	var z=10;
	print(z); //10
	print("5+5:$z");  //$is used to add respected values in strings if there is single values then no need to ake curlybrackets if more then 1 then add in curly brackets
	print('5+5+5: ${z+5}');
   		
	var str="Abhishek";
	print(str);  //Abhishek
	
	//var is taking runtime data type 
	
	int a=10;
	print(a);      // compulasary we have to give the velue if we are using it if not accessing then it works
	
	double b=20.00;
	print(b);
	
	String strg= 'Hassan';
	print(strg);
	
	//runtimeType is the property which gives the runtime type of data
	print(strg.runtimeType);
	print(b.runtimeType);
	print(a.runtimeType);
	print(str.runtimeType);
	print(z.runtimeType);
	print(x.runtimeType);
	
	bool f=false;
	print(f);
	
	a=25;  // also i can change the values
	print(a);
	
}
