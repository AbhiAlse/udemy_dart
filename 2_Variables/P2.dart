//final and constant keyword

void main(){
	int x=10;
	print(x);
	x=20;
	print(x);

	final int a=25;  //here final means the value is assigned and cannot be changed
	print(a);
	a=30;  //cant assign the final variables x
	print(a);

	const int b=25;  //here const means the value is assigned and cannot be changed
	print(b);
	b=30;  //cant assign the const variables x
	print(b);

}
