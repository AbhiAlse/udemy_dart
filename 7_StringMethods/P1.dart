/*
	String properties and methods:
		isEmpty
		isNotEmpty
		toLowercase()
		toUppercase()
		contains("xyz")
		replaceRange(start,end,"String")
		replaceFirst('Word',"String")
		replaceAll('Word',"String")
		
		x.split('Word')   //will return the string by removing that word send by arguments
		x.join('word')
		x.trim()

	

*/
