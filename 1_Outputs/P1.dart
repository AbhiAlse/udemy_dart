//Printing Output in following ways

void main(){
	print("Hello Abhishek");
	print('Hello Pune ');
	//use to print multilines 
	print('''

		Hello World!


	''');
	// $ is used to display values inside it
	print('3+5= ${3+5}');
	print('3+5 = ${3+5}');
}
